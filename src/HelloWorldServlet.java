import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

/**
 * Created by majab on 04.09.2017.
 */

public class HelloWorldServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fooValue = req.getParameter("foo");

        ServletOutputStream out = resp.getOutputStream();
        //Erzeuge Heading
        out.println("<h1>Hello World!</h1>");
        //erzeuge textzeile
        out.println("<span>Der Wert des Parameter foo war " + fooValue + "</span>");

        Speisekarte k = new Speisekarte();
        List<String> list =k.getSpeisen();
        erzeugeListe(out, list);

    }

    private void erzeugeListe(ServletOutputStream out, List<String> list) throws IOException {
        //Erstelle eine HTML-Liste aus der list-Variable
        out.println("<u1>");

        for(String s: list){
            out.println("<li>" + s + "</li>");//Erzeuge List Item für das aktuelle Element der Schleife
        }

        out.println("</u1>");
    }

}
