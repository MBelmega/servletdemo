import java.util.Arrays;
import java.util.List;

/**
 * Created by majab on 04.09.2017.
 */
public class Speisekarte {
    public List<String> getSpeisen() {
        return Arrays.asList("Currywurst", "Kaviar", "Le Deneur");
    }
}
